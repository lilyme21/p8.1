//////////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 9/25/19
// This program will take 3 numbers from the use as the amount
// of ticks to turn a lock right, left, then right. If the
// amount of ticks match the fixed combo, the program will
// print that the lock has been opened is true or false.
//////////////////////////////////////////////////////////////

import java.util.Scanner;                       // importing package to use scanner

public class P8_1 {
    public static class ComboLock {
        // initializing attributes
        private int[][] combo = new int[3][2];  // to hold fixed combination and user input combo
        private int[] attempt = new int[3];     // to hold user input dial turn
        private int dial;                       // to hold place of dial
        // constructor to set lock combo and start the dial at 0
        public ComboLock(int s1, int s2, int s3) {
            // letting the user know that the dial will start at zero for the first turn
            System.out.println("The dial of the lock starts at zero. \n");
            // setting the attribute combo to the lock combo
            combo[0][0] = s1;
            combo[1][0] = s2;
            combo[2][0] = s3;
            // setting the dial to 0
            dial = 0;
        }
        // method to reset the dial if it goes over 39 or below 0
        public void reset() {

            if (dial >= 40)
                dial -= 40;     // correcting the dial if it reaches 40 or above
            else if (dial <= -1)
                dial += 40;     // correcting the dial if it falls below 0
        }
        // method to turn the dial left
        public void turnLeft(int ticks) {
            dial += ticks;      // adds the amount of ticks to the dial
            reset();            // correcting the dial if necessary
        }
        // method to turn the dial right
        public void turnRight(int ticks) {
            dial -= ticks;      // subtracts the amount of ticks from the dial
            reset();            // correcting the dial if necessary
        }
        // method to check if the lock is opened by user input combo
        public boolean open() {
            // prompting user to enter 3 numbers to turn the dial right, left, right
            System.out.println("Please enter three numbers to turn the dial right, left, right: ");
            // saving the first line of user input to String userIn
            Scanner in = new Scanner(System.in);
            String userIn = in.nextLine();
            // converting String userIn to array of Strings
            String[] stringArray = userIn.split(" ");
            // converting first 3 Strings of array stringArray to int array
            for (int i = 0; i < 3; i++)
                attempt[i] = Integer.parseInt(stringArray[i]);
            // calling method turnRight for users first integer
            turnRight(attempt[0]);
            // setting combo[0][1] to value in dial set by previous line
            combo[0][1] = dial;
            // calling method turnLeft for users second integer
            turnLeft(attempt[1]);
            // setting combo[1][1] to value in dial set by previous line
            combo[1][1] = dial;
            // calling method turnRight for users third integer
            turnRight(attempt[2]);
            // setting combo[2][1] to value in dial set by previous line
            combo[2][1] = dial;
            // checking if the set combo and the users combo match
            if ((combo[0][0] == combo[0][1]) && (combo[1][0] == combo[1][1]) && (combo[2][0] == combo[2][1]))
                return true;        // return true if matched
            else
                return false;       // return false if not
        }

    }
    public static void main(String[] args) {
        // constructing new lock with combination 10 15 30
        ComboLock myLock = new ComboLock(10, 15, 30);
        // calling method open() to check if lock is open and showing the necessary input for open() to return true
        System.out.println("The lock is open: " + myLock.open() +
                "\nShould be true if chose numbers 30 5 25.\nShould be false otherwise.");
    }
}
